# README #

UnityActorSimulator tests.

### What is this repository for? ###

* This is a simple Unity project that uses the UnityActorSimulator.dll in order to show its usage.
Moreover, it defines some useful and basic c# script related to 2D movement and world interaction (sonar script).
For more info about the UnityActorSimulator.dll, please check https://bitbucket.org/hl2exe/unity-actor-simulator

* Version: 1.00

### Usage ###

* Just download the project and open it via the Unity Editor
* Dependencies: UnityActorSimulator.dll, UniRx, UnityProlog (see https://github.com/ianhorswill/UnityProlog)

### Contribution guidelines ###

* Code review: the code may appear very simple and toy-like. Please feel free to send me an email for any sort of issue at:
federugg26@gmail.com or federico.ruggeri4@studio.unibo.it